using Godot;

public class Item : Object {
    [Export] public string Type;

    public Item(string type) {
            Type = type;
    }
}