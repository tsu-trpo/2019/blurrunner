using Godot;

public class ItemPicker : Area2D {
    Inventory _holdInv;

    public void Init(Inventory inv) {
        _holdInv = inv;
    }

    public void Pick() {
        foreach (PickableItem one in GetOverlappingAreas()) {
            if (_holdInv.TryAdd(one.Item)) {
                one.QueueFree();
            }
        }
    }
}
