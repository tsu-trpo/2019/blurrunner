using Godot;
using System;

public class EnemySoul : Node2D 
{
    [Export] int _minSpeed = 50;
    [Export] int _maxSpeed = 125;

    float _time = 0;
    Vector2 _velocity = new Vector2();
    Body _body;


    public override void _Ready() 
    {
        _body = GetParent<Body>();
    }
    
    void RandomizeNavigation()
    {
        var random  = new Random();

        var direction = new Vector2();
        direction.x = random.Next(-1, 2);
        direction.y = random.Next(-1, 2);

        int speed = random.Next(_minSpeed, _maxSpeed + 1);

        _velocity = direction.Normalized() * speed;
    }

    void IdleMoving(float delta)
    {
        _time += delta;
        if (_time <= 1.5) 
        {
            _velocity = _body.MoveAndSlide(_velocity);
        }
        
        if (_time >= 4.0)
        {
            _time -= 4.0f; 
            RandomizeNavigation();
        }
    }

    public override void _PhysicsProcess(float delta) 
    {
        IdleMoving(delta);
    }
}
