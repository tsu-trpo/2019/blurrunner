using Godot;
using System;
public class RoomTextures
{
    public static int right_wall = 0;
    public static int wall = 1;
    public static int floor = 2;
}

public class Room : Node2D
{
    [Export]
    ushort max_room_size;

    private enum Type {
        Square,
        Rectangle
    }

    public override void _Ready() {
        TileMap = GetNode<Godot.TileMap>("Room_TileMap");
        Generate_room(GetRandomType());
    }

    Room.Type GetRandomType() {
        var count = Enum.GetNames(typeof(Room.Type)).Length;
        Random rand = new Random();
        return (Room.Type)rand.Next(count);
    }

    void Generate_room(Room.Type t_room) {
        Vector2 room_size = Generate_room_size(t_room);
        Generate_parimeter(room_size);
        Generate_inner(room_size);
    }

    Vector2 Generate_room_size(Room.Type t_room) {
        Vector2 border_room_size = new Vector2(1,1);
        Vector2 inner_room_size = new Vector2(max_room_size, max_room_size);

        if (t_room == Room.Type.Rectangle) {
                Random rnd = new Random();
                int x_longer = rnd.Next(0,2);
                switch (x_longer) {
                    case 0:
                        inner_room_size.x = max_room_size/rnd.Next(2,6);
                        break;
                    case 1:
                        inner_room_size.y = max_room_size/rnd.Next(2,6);
                        break;
                }
        }

        Vector2 room_size = inner_room_size + 2 * border_room_size;
        return room_size;
    }

    void Generate_parimeter(Vector2 size) {
        bool flip_x = true;
        for (int y = 0; y < size.y; y++) {
            TileMap.SetCell(0, y, RoomTextures.right_wall, flip_x);
            TileMap.SetCell((int)size.x - 1, y, RoomTextures.right_wall);
        }
        for (int x = 0; x < size.x; x++) {
            TileMap.SetCell(x, 0, RoomTextures.wall);
            TileMap.SetCell(x, (int)size.y - 1, RoomTextures.wall);
        }
    }

    void Generate_inner(Vector2 size) {
        for (int x = 1; x < size.x - 1; x++) {
            for (int y = 1; y < size.y - 1; y++) {
                TileMap.SetCell(x, y, RoomTextures.floor);
            }
        }
    }
    
    Godot.TileMap TileMap;

}
