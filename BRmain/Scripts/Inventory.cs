using Godot;

public class Inventory : Object {
    int _capacity = 8;
    Item[] _cells;
    [Signal] delegate void InventoryUpdated();

    public Inventory() {
        _cells = new Item[_capacity];
    }

    public bool TryAdd(Item item) {
        for (int i = 0; i < _capacity; i++) {
            if (_cells[i] != null) {
                continue;
            }
            _cells[i] = item;
            EmitSignal("InventoryUpdated");
            return true;
        }
        return false;
    }

    public Item this[int idx] {
        get{ return _cells[idx]; }
    }

    public int Capacity {
        get{ return _capacity; }
    }
}