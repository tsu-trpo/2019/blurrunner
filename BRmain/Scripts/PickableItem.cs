using Godot;

public class PickableItem : Area2D {
    [Export] public string ItemType;
    Sprite _sprite;
    Item _item;

    public override void _Ready() {
        if (ItemType == null) {
            QueueFree();
        }
        _item = new Item(ItemType);
        _sprite = GetNode<Sprite>("Sprite");
        _sprite.Texture = GD.Load("res://Sprites/" + _item.Type + ".png") as Texture;
    }

    public Item Item {
        get { return _item; }
    }
}