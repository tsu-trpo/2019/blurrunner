using Godot;

public class InvenoryBarCell : TextureRect {
    TextureRect _itemIcon;

    public override void _Ready() {
        _itemIcon = GetNode<TextureRect>("TextureRect");
    }

    public void FillWith(Item item) {
        if (item == null) {
            _itemIcon.Texture = null;
        }
        else {
            _itemIcon.Texture = GD.Load("res://Sprites/" + item.Type + ".png") as Texture;
        }
    }
}
