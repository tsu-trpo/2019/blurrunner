using Godot;

public class BodySettler : Area2D {
    Sprite _circle;
    Soul _soul;
    Body _target;

    public void Init(Soul soul) {
        _soul = soul;
        _circle = GetNode<Sprite>("Circle");
    }

    public void StartResettle() {
        _circle.Show();
    }

    public void FinishResettle() {
        _circle.Hide();
        if (GetOverlappingBodies().Contains(_target)) {
            if (_target.BodyType == Body.Type.Enemy) {
                _soul.SettleIn(_target);
            }
        }
    }

    public void SetTarget(Node2D target) {
        _target = target as Body;
    }
}