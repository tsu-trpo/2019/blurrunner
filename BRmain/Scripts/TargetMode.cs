using Godot;

public class TargetMode : Object {
    Soul _soul;
    Body _target;
    Node2D _boarder;
    Area2D _searcher;

    public TargetMode(Soul soul, Mouse mouse) {
        _soul = soul;
        _searcher = Resources.MakePassiveTarget() as Area2D;
        _boarder = Resources.MakeActiveTarget();
        mouse.AddChild(_searcher);
        _soul.UpdateTarget(_searcher);
    }

    public void StartCapture() {
        Refuse();
        _searcher.Show();
    }

    public void FinishCapture() {
        _searcher.Hide();
        foreach (Body aim in _searcher.GetOverlappingBodies()) {
            if (_soul.Body == aim || _target == aim) {
                continue;
            }
            _soul.UpdateTarget(_target = aim);
            _target.AddChild(_boarder);
            _target.Connect("BodyDies",this,"Refuse");
        }
    }

    public void CheckTarget() {
        if (_soul.Body == _target) Refuse();
    }

    void Refuse() {
        _soul.UpdateTarget(_searcher);
        if (_target == null) {
            return;
        }
        _target.RemoveChild(_boarder);
        _target.Disconnect("BodyDies",this,"Refuse");
        _target = null;
    }
}