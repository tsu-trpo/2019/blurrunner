using Godot;

public class World : Node2D {
    Body _body;
    Soul _soul;
    Inventory _inventory;
    InventoryBar _inventoryBar;

    public override void _Ready() {
        FindDependencies();
        _soul.Init(_inventory,GetNode<Mouse>("Mouse"));
        _soul.SettleIn(_body);
        _inventoryBar.Init(_inventory);
    }

    void FindDependencies() {
        _soul = Resources.MakeSoul();
        _body = GetNode<Body>("Body");
        _inventory = new Inventory();
        _inventoryBar = GetNode<InventoryBar>("GUI/InventoryBar");
    }
}