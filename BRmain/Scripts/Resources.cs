using Godot;

public static class Resources {
    static PackedScene _soulLoad = ResourceLoader.Load<PackedScene>("res://Scenes/Soul.tscn");
    static PackedScene _invenoryBarCellLoad = ResourceLoader.Load<PackedScene>("res://Scenes/InventoryBarCell.tscn");
    static PackedScene _passiveTargetLoad = ResourceLoader.Load<PackedScene>("res://Scenes/PassiveTarget.tscn");
    static PackedScene _activeTargetLoad = ResourceLoader.Load<PackedScene>("res://Scenes/ActiveTarget.tscn");

    public static Soul MakeSoul() {
        return _soulLoad.Instance() as Soul;
    }

    public static InvenoryBarCell MakeBarCell() {
        return _invenoryBarCellLoad.Instance() as InvenoryBarCell;
    }

    public static Node2D MakePassiveTarget() {
        return _passiveTargetLoad.Instance() as Node2D;
    }

    public static Node2D MakeActiveTarget() {
        return _activeTargetLoad.Instance() as Node2D;
    }
}