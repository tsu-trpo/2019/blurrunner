using Godot;

public class FightMode : Node {
    Weapon _weapon;
    Node2D _target;

    public override void _Process(float delta) {
        _weapon.LookAt(_target.GetGlobalPosition());
    }

    public void ObtainBody(Body body) {
        _weapon = body.Weapon;
    }

    public void PerformAttack() {
        _weapon.AnimateHit();
        foreach (Body body in _weapon.GetOverlappingBodies()) {
            body.ChangeHealth(-_weapon.Damage);
        }
    }

    public void SetTarget(Node2D target) {
        _target = target;
    }
}