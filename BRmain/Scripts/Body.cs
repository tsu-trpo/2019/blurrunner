using Godot;

public class Body : KinematicBody2D {
    public Type BodyType = Type.Enemy;
    int _health = 100;
    Soul _soul;
    Weapon _weapon;
    [Signal] delegate void HealthUpdated();
    [Signal] delegate void BodyDies();

    public override void _Ready() {
        _weapon = GetNode<Weapon>("Weapon");
        GetNode<BodyGUI>("BodyGUI").Init(this);
        EmitSignal("HealthUpdated",_health);
    }

    public void ChangeHealth(int hpDelta) {
        EmitSignal("HealthUpdated",_health += hpDelta);
        if (_health <= 0) {
            Die();
        }
    }

    public void AdoptSoul(Soul soul) {
        AddChild(_soul = soul);
        BodyType = Type.Player;
    }

    public void AbortSoul() {
        RemoveChild(_soul);
        BodyType = Type.Enemy;
    }

    public Weapon Weapon {
        get { return _weapon; }
    }

    void Die() {
        EmitSignal("BodyDies");
        QueueFree();
    }

    public enum Type {
        Player,
        Enemy,
    }
}