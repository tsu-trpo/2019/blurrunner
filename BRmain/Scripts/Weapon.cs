using Godot;

public class Weapon : Area2D {
    AnimationPlayer _animation;
    int _damage = 10;

    public override void _Ready() {
        _animation = GetNode<AnimationPlayer>("AnimationPlayer");
    }

    public void AnimateHit() {
        _animation.Play("Hit");
    }

    public int Damage {
        get{ return _damage; }
    }
}
