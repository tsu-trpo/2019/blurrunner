using Godot;

public class InventoryBar : Control {
    Inventory _inventory;
    Godot.Collections.Array<InvenoryBarCell> _cells = new Godot.Collections.Array<InvenoryBarCell>();

    public void Init(Inventory inv) {
        _inventory = inv;
        _inventory.Connect("InventoryUpdated", this, "UpdateCells");

        var barCellsHolder = GetNode<HBoxContainer>("TextureRect/ItemHolder");
        
        for(int i=0; i < _inventory.Capacity; i++) {
            var cell = Resources.MakeBarCell();
            _cells.Add(cell);
            barCellsHolder.AddChild(cell);
        }
    }

    public void UpdateCells() {
        for(int i=0; i < _inventory.Capacity; i++) {
            _cells[i].FillWith(_inventory[i]);
        }
    }
}