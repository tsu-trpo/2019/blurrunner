using Godot;

public class Mouse : Node2D {
    public override void _Process(float delta) {
        SetPosition(GetGlobalMousePosition());
    }
}
