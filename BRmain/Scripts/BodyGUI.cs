using Godot;

public class BodyGUI : Control {
    Label _healthBar;

    public void Init(Body parent) {
        parent.Connect("HealthUpdated",this,"ChangeValue");
        _healthBar = GetNode<Label>("HealthBar");
    }

    public void ChangeValue(int value) {
        _healthBar.SetText(value.ToString());
    }
}
