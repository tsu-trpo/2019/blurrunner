using Godot;

public class Soul : Node2D {
    Body _body;
    FightMode _fightMode;
    TargetMode _targetMode;
    ItemPicker _itemPicker;
    BodySettler _bodySettler;
    Vector2 _velocity = new Vector2();
    int _speed = 400;

    public void Init(Inventory inv, Mouse mouse) {
        _itemPicker = GetNode<ItemPicker>("ItemPicker");
        _itemPicker.Init(inv);

        _bodySettler = GetNode<BodySettler>("BodySettler");
        _bodySettler.Init(this);

        _fightMode = GetNode<FightMode>("FightMode");
        _targetMode = new TargetMode(this,mouse);
    }

    public override void _PhysicsProcess(float delta) {
        _velocity = _body.MoveAndSlide(_velocity);
    }

    public override void _Input(InputEvent @event) {
        MovementInput();
        TargetInput();
        BodySettlerInput();
        BattleInput();
        ItemPickerInput();
    }

    public void SettleIn(Body b) {
        if (_body != null) {
            _body.AbortSoul();
        }
        _body = b;
        _body.AdoptSoul(this);
        EmitBodyUpdated();
    }

    public Body Body {
        get{ return _body; }
    }

    public void UpdateTarget(Node2D target) {
        _fightMode.SetTarget(target);
        _bodySettler.SetTarget(target);
    }

    void EmitBodyUpdated() {
        _fightMode.ObtainBody(_body);
        _targetMode.CheckTarget();
    }

    void MovementInput() {
       _velocity = new Vector2();
        if (Input.IsActionPressed("ui_right")) _velocity.x += 1;
        if (Input.IsActionPressed("ui_left")) _velocity.x -= 1;
        if (Input.IsActionPressed("ui_down")) _velocity.y += 1;
        if (Input.IsActionPressed("ui_up")) _velocity.y -= 1;
        _velocity = _velocity.Normalized() * _speed;
    }

    void TargetInput() {
        if (Input.IsActionJustPressed("RightClick")) _targetMode.StartCapture();
        if (Input.IsActionJustReleased("RightClick")) _targetMode.FinishCapture();
    }

    void BodySettlerInput() {
        if (Input.IsActionJustPressed("Settle")) _bodySettler.StartResettle();
        if (Input.IsActionJustReleased("Settle")) _bodySettler.FinishResettle();
    }

    void BattleInput() {
        if (Input.IsActionJustPressed("LeftClick")) _fightMode.PerformAttack();
    }

    void ItemPickerInput() {
        if (Input.IsActionJustPressed("ui_select")) _itemPicker.Pick();
    }
}